import java.util.HashMap;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author carli
 */
public class TokenWords extends HashMap<String, TokenType> {
    
    public TokenWords() {
           

        this.put("(", TokenType.LPAR);
        this.put(")", TokenType.RPAR);
        this.put(";", TokenType.SEMI);
        this.put(",", TokenType.COMA);
        this.put(".", TokenType.DOT);
        this.put(":", TokenType.SEMIC);
        this.put("[", TokenType.LBRA);
        this.put("]", TokenType.RBRA);
        this.put("+", TokenType.PLUS);
        this.put("-", TokenType.MINUS);
        this.put("=", TokenType.EQUAL);
        this.put("<", TokenType.LGREAT);
        this.put(">", TokenType.RGREAT);
        this.put("<=", TokenType.LGREATE);
        this.put(">=", TokenType.RGREATE);
        this.put("<>", TokenType.NOTEQUAL);
        this.put("*", TokenType.MULT);
        this.put("/", TokenType.SLASH);
        this.put("and", TokenType.AND);
        this.put("array", TokenType.ARRAY);
        this.put("begin", TokenType.BEGIN);
        this.put("div", TokenType.DIV);
        this.put("do", TokenType.DO);
        this.put("else", TokenType.ELSE);
        this.put("end", TokenType.END);
        this.put("funciton", TokenType.FUNCTION);
        this.put("if", TokenType.IF);
        this.put("integer", TokenType.INTEGER);
        this.put("mod", TokenType.MOD);
        this.put("not", TokenType.NOT);
        this.put("of", TokenType.OF);
        this.put("or", TokenType.OR);
        this.put("procedure", TokenType.PROCEDURE);
        this.put("program", TokenType.PROGRAM);
        this.put("read", TokenType.READ);
        this.put("real", TokenType.REAL);
        this.put("return", TokenType.RETURN);
        this.put("then", TokenType.THEN);
        this.put("var", TokenType.VAR);
        this.put("while", TokenType.WHILE);
        this.put("write", TokenType.WRITE);

        
    };
}
