

/**
 * This enum class contains all the keywords for the mini pascal language
 * Those are in 4 categories:
 * Keywords, Symbols, ID and Number
 * @author carli
 */
public enum TokenType {
    
    AND, 
    ARRAY, 
    BEGIN, 
    COMA, 
    DIV, 
    DO, 
    DOT, 
    ELSE, 
    END, 
    EQUAL, 
    FUNCTION, 
    ID, 
    IF, 
    INTEGER, 
    LBRA, 
    LGREAT, 
    LPAR, 
    MINUS, 
    MOD, 
    MULT, 
    NOT, 
    OF, 
    OR, 
    PLUS, 
    PROCEDURE, 
    PROGRAM, 
    RBRA, 
    READ, 
    REAL, 
    RETURN, 
    RGREAT, 
    RPAR, 
    SEMI, 
    SEMIC, 
    SLASH, 
    THEN, 
    VAR, 
    WHILE, 
    WRITE, 
    LGREATE, 
    RGREATE, 
    NOTEQUAL
   
}
