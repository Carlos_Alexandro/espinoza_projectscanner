import java.util.HashMap;
import java.io.StringReader;
/**
 * This is a simple example of a jflex lexer definition
 * that is not a standalone application
 */

/* Declarations */


%%

%class  CarlosScanner   /* Names the produced java file */
%function nextToken  /* Renames the yylex() function */
%type   Token      /* Defines the return type of the scanning function */
%{
  private TokenWords lookupTable = new TokenWords();
%}


%eofval{
  return null;
%eofval}
/* Patterns */


letter        = [A-Za-z]
whitespace    = [ \r\n\t\f ]*
number      = [0-9]
numbers		  = {number}{number}*
ID          = {letter}({letter}|{number})*
symbols		  = [";"|","|"'"|"."|":"|"["|"]"|"("|")"|"+"|"-"|"="|"<>"|"<"|"<="|">"|">="|"*"|"/"|":="]
comments    = [/**/]
other         = .

%%
/* Lexical Rules */

// Does not do anything if it finds white space            
{whitespace}  {  /* Ignore Whitespace */  }

// Returns a single letter if its found just a singular char.
{letter}	{
  Token token = new Token(yytext(), TokenType.ID);
	// Returns a letter if its the standalone char.
  return(token);
}

// This will return the values of the yytext function plus the token type
// which is symbols if it matches any of the symbols in the look up table
{symbols}	{
  String tokenText = yytext();
  return(new Token(tokenText, lookupTable.get(tokenText)));
}


// This will return the token type plus the data within the
// token with the token type being the ID of a string of characters.
// This means it could contain numbers and letters and have a larger
// range of things that could be read within the scanner.
{ID} {
  // Since I have to use the value from the yytext function
  // More than once, I store it in a string so I can use it
  // Multiple times.
  String tokenText = yytext();
  // This assigns that token type from the lookup table if it matches
  // any of the keywords
  TokenType tt = lookupTable.get(tokenText); 
  // If it doesn't, this means that it will return whatever its fed.
  if(tt == null){
    tt = TokenType.ID;
  }
  return(new Token(tokenText, tt));
}

// This will return the token type plus the data within the
// token with the token type being a Number.
{number} {
  return(new Token(yytext(), TokenType.INTEGER));
}

// Anything that is categorized as other, therfore not found in
// the list of keywords or patterns, will be consider illegal
// characters.
{other} { 
   System.out.println("Illegal char: '" + yytext() + "' found.");
}