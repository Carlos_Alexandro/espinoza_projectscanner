/**
 *  A simple Token class containing only a String.
 */
public class Token
{
    private String lexeme;
    private TokenType type;    

    public Token(String l, TokenType t)
    {
        this.lexeme = l;
        this.type = t;
    
    };
    
    public String getLexeme(){
        return this.lexeme;
    }

    public TokenType getType(){
        return this.type;
    }

    public String toString() { 
        return "Token: " + this.type + "    Content: " + this.lexeme;
        //return "Content: " + this.lexme;
    }

}